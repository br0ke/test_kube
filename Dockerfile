FROM python:3.7.0-alpine3.8

RUN mkdir /app
WORKDIR /app
ADD requirements.txt .
RUN pip install -r requirements.txt
ADD . .
EXPOSE 5000
CMD ["python", "server.py"]

import os
from time import gmtime, strftime

from flask import Flask, request


port = os.getenv('PORT', 5000)
app = Flask(__name__)


@app.route("/")
def hello():
    return (
        f"<h1>{os.uname()[1]}</h1>"
        f"Current time: {strftime('%Y-%m-%d %H:%M:%S', gmtime())}<br/>"
        f"💀 <a href=\"/kill\">Kill an instance of the app</a>"
    )

@app.route("/kill")
def kill():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=port, passthrough_errors=True)
